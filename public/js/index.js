$(document).ready(function(){
    $("a.thumbnail").on("click", function(evt){
        var ctx = this;
        var img = $(ctx).find("img");
        var src = img.attr("src");
        var filename = img.data("filename");

        var tmpl = _.template($("#modal").html());
        var content = tmpl({
            img_src     : src,
            title       : "Sharing image: Car " + filename,
            filename    : filename,
            description : "This car has color " + filename
        });

        $.fancybox({
            content: content
        });

        evt.preventDefault();
    });

    //magic!
    $(document).on("click", ".share", function(evt){
        evt.preventDefault();
        var title       = $(this).data("title");
        var filename    = $(this).data("filename");
        var description = $(this).data("description");

        FB.ui({
            method: "share_open_graph",
            action_type: "og.shares",
            action_properties: JSON.stringify({
                object:{
                    "og:url": HOST,
                    "og:title": title,
                    "og:description": description,
                    'og:og:image:width': '400',
                    'og:image:height': '400',
                    'og:image': HOST + '/img/'+ filename +'.jpg'
                }
            })
        });

    });
});
